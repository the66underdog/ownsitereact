import exp from 'express';
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import multer from 'multer';
import mysql from 'mysql2';

const conn = mysql.createPool({
    host: 'localhost',
    user: 'root',
    database: 'macos_chat',
});

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname + '/../' + '../' + 'macos-chat/' + 'build/' + 'Images/'))
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = encodeURI(file.originalname);
        cb(null, uniqueSuffix)
    }
})
let app = exp();
let upload = multer({ storage: storage });

app.use(exp.json());

app.get('/servers', selectServers, (req, res) => {
    if (req.result)
        res.send(JSON.stringify(req.result));
    else {
        res.status(500).send(JSON.stringify('No servers yet, feel free to make one'));
    }
    res.end();
});

function selectServers(req, res, next) {
    conn.query('SELECT * FROM servers', (err, result) => {
        err && next(err);
        req.result = result;
        next();
    });
}

app.post('/servers', upload.single('server-ava'), (req, res, next) => {
    req.obJSON = {
        'id': undefined,
        'ava': req.file.filename,
        'name': req.body['server-name'],
        'privacy': req.body['server-privacy'] ? 1 : 0,
        'favorite': 0,
    }
    next();
}, createServerTable, defineId, addServer)

function createServerTable(req, res, next) {
    conn.query(`CREATE TABLE servers (
        id SMALLINT NOT NULL,
        ava VARCHAR(255),
        name VARCHAR(255),
        privacy BOOL,
        favorite BOOL,
        PRIMARY KEY(id)
    )`, err => {
        err && err.code === 'ER_TABLE_EXISTS_ERROR' ? console.log('Table already exists') : next(err);
        next();
    });
}

function defineId(req, res, next) {
    conn.query('SELECT id FROM servers', (err, result) => {
        if (err)
            next(err);
        else {
            let maxId = 0;
            if (!result.length)
                req.obJSON.id = 0;
            else {
                for (let item of result) {
                    item.id > maxId && (maxId = item.id);
                }
                for (let i = 0; i < maxId; i++) {
                    if (!(result.filter(item => (item.id === i)).length)) {
                        req.obJSON.id = i;
                        break;
                    }
                }
            }
            req.obJSON.id === undefined && (req.obJSON.id = maxId + 1);
            next();
        }
    });
}

function addServer(req, res, next) {
    const arrBuf = [];
    for (let key in req.obJSON) {
        arrBuf.push(req.obJSON[key]);
    }
    conn.execute('INSERT INTO servers VALUES (?, ?, ?, ?, ?)', arrBuf, err => {
        err && next(err);
        res.send(JSON.stringify(req.obJSON));
    });
}

app.patch('/servers/:id', (req, res, next) => {
    req.body.id = +req.params.id;
    next();
}, toggleFavoriteServer, (req, res, next) => {
    conn.execute('SELECT * FROM servers', (err, result) => {
        err ? next(err) : res.send(JSON.stringify(result));
    })
});

function toggleFavoriteServer(req, res, next) {
    conn.execute(`SELECT favorite from servers WHERE id = ${req.body.id}`, (err, result1) => {
        if (err)
            next(err);
        else {
            conn.execute(`UPDATE servers SET favorite = ${result1[0].favorite ? 0 : 1} WHERE id = ${req.body.id}`, err => {
                err && next(err);
                next();
            })
        }
    });
}

app.put('/servers/:id', (req, res, next) => {
    req.body.id = +req.params.id;
    next();
}, deleteServer, (req, res, next) => {
    res.end();
});

function deleteServer(req, res, next) {
    conn.execute(`DELETE FROM servers WHERE id = ${req.body.id}`, err => {
        err && next(err);
        next();
    })
}

app.use((req, res) => {
    if (req.method === 'GET') {
        if (req.url === '/')
            loadPage(res, 'index.html', 'text/html');
        else {
            loadPage(res, req.url, getContentType(req.url));
        }
    }
});

app.listen(3001, (err) => {
    if (err)
        console.log('Error:' + err);
    else
        console.log(`It' fine`);
});

app.use(errorHandler);

function loadPage(res, name, conType) {
    const file = path.join(__dirname + '/../' + '../' + 'macos-chat/' + 'build/' + name);
    fs.readFile(file, (err, data) => {
        if (err)
            res.writeHead(404);
        else {
            res.writeHead(200, { 'Content-Type': conType });
            res.write(data);
        }
        res.end();
    })
}

function getContentType(url) {
    let ext = path.extname(url);
    switch (ext) {
        case '.html':
            return 'text/html';
        case '.css':
            return 'text/css';
        case '.js':
            return 'text/javascript';
        case '.json':
            return 'application/json';
        default:
            return 'multipart/form-data';
    }
}

function errorHandler(err, req, res, next) {
    switch (err.code) {
        case 'ER_BAD_DB_ERROR':
        case 'ER_NO_SUCH_TABLE':
            res.status(500).send(JSON.stringify('Create a server to start chatting'));
            break;
        case 'ECONNREFUSED':
            res.status(500).send(JSON.stringify('Web-server is probably turned off, contact dev: @the66underdog (telegram, vk), Ha-Hyena#0290 (discord)'));
            break;
        default:
            res.status(500).send(JSON.stringify(err.message));
            break;
    }
}
import React from 'react';
import './style.css';

function ListChat() {



    return (
        <div className="list-chat">
            <div className="margin-controller">
                <div className="name-server">
                    <h1>Nomad List</h1>
                    <img src="Images/Arrow.png" className="arrow" alt='arrow' />
                </div>
                <div className="settings">
                    <img src="Images/Gear.png" alt='settings' />
                </div>
                <div className="treads">
                    <img src="Images/Msg.png" alt='msg' />
                    <span>All treads</span>
                </div>
                <div className="header-channels">
                    <span>channels</span>
                    <span className="number-block"></span>
                </div>
            </div>
            <nav>
                <ul className="list-channels">
                    <li>general</li>
                    <li>support</li>
                    <li>marketing</li>
                    <li>thailand</li>
                    <li>bali</li>
                    <li>poland</li>
                    <li>australia</li>
                    <li>jobs</li>
                    <li>startups</li>
                    <li>italy</li>
                    <li>freelance</li>
                </ul>
            </nav>
            <div className="margin-controller">
                <div className="header-friends">
                    <span>friends</span>
                    <span className="number-block"></span>
                </div>
            </div>
            <ul className="list-friends">
                <li>
                    <div className="user-status"></div>
                    <div data-num="n0000"></div>
                    <span className="user-name" data-usname="@DiggidyDiggy" data-email="Diggs@gmail.com"
                        data-skype="GoalDigger322">
                        Orlando Diggs
                    </span>
                </li>
                <li>
                    <div className="user-status"></div>
                    <div data-num="n0001"></div>
                    <span className="user-name" data-usname="@CarButManWhoIam" data-email="CarVelasco@icloud.com"
                        data-skype="KarmaVelasco">
                        Carmen Velasco
                    </span>
                </li>
                <li>
                    <div className="user-status"></div>
                    <div data-num="n0002"></div>
                    <span className="user-name" data-usname="@RedishMarieMaker" data-email="MarieRed@gmail.com"
                        data-skype="Jensen48NotFloor">
                        Marie Jensen
                    </span>
                </li>
                <li>
                    <div className="user-status"></div>
                    <div data-num="n0003"></div>
                    <span className="user-name" data-usname="@AlexA1st" data-email="a1Alex@gmail.com" data-skype="-">
                        Alex Lee
                    </span>
                </li>
                <li>
                    <div className="user-status"></div>
                    <div data-num="n0004"></div>
                    <span className="user-name" data-usname="@GillValentine" data-email="VaGillentine@icloud.com"
                        data-skype="LeoTheTurrtle">
                        Leo Gill
                    </span>
                </li>
                <li>
                    <div className="user-status"></div>
                    <div data-num="n0005"></div>
                    <span className="user-name" data-usname="@BritneyNotSpearce" data-email="brittyBritney@gmail.com"
                        data-skype="BrittyBritney">
                        Britney Cooper
                    </span>
                </li>
            </ul>
        </div>
    );
}

export default ListChat;
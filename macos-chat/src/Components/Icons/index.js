/* eslint-disable jsx-a11y/alt-text */
import React, { createContext, memo, useEffect } from 'react';
import './style.css'
import MacosDots from './Dots';
import ServersList from './ServersList';
import AddServer from './AddServer';
import { useDispatch } from 'react-redux';
import { setServers } from '../../Store/Reducers/serversReducer.js';

export const SetFavorite = createContext();

const MemAddServer = memo(AddServer);

function Icons() {

    const dispatch = useDispatch();

    useEffect(() => {
        const controller = new AbortController();
        async function getServers() {
            try {
                const res = await fetch('/servers', {
                    signal: controller.signal
                });
                const result = await res.json();
                res.ok ? dispatch(setServers(result)) : console.log(result);
            }
            catch (err) {
                err.name !== 'AbortError' && console.log(err.message);
            }
        }
        getServers();
        return () => { controller.abort() };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <>
            <div className="icons">
                <MacosDots />
                <ServersList />
            </div>
            <MemAddServer />
        </>
    );
}

export default Icons;
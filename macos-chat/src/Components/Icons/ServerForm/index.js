import React, { useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { addServers } from '../../../Store/Reducers/serversReducer';
import './style.css';

function ServerForm({ isDisp }) {

    const dispatch = useDispatch();

    const refAva = useRef();

    const [ava, setAva] = useState(`${process.env.PUBLIC_URL}/Images/Cross.png`);

    async function postServer(form) {
        try {
            const formData = new FormData(form);
            const res = await fetch('/servers', {
                method: 'POST',
                body: formData,
            });
            if (res.ok) {
                const result = await res.json();
                dispatch(addServers(result));
            }
            else {
                const err = await res.text();
                throw err;
            }
        }
        catch (err) {
            console.log(err);
        }
    }

    function showAvatar(event, rA) {
        const blob = new Blob([event.target.files[0]]);
        const fReader = new FileReader();
        fReader.readAsDataURL(blob);
        fReader.onload = () => {
            setAva(fReader.result);
            rA.current.classList.add('custom');
        }
    }

    function showPlus(rA) {
        setAva(`${process.env.PUBLIC_URL}/Images/Cross.png`);
        rA.current.classList.remove('custom');
    }

    return (
        <div className='form-background' style={{ visibility: isDisp ? 'visible' : 'hidden', backgroundColor: isDisp ? 'rgba(64, 64, 96, 0.5)' : 'rgba(0, 0, 0, 0)' }}>
            <form className='server-form' method='POST' style={{ opacity: isDisp ? '1' : '0' }}
                onSubmit={e => { e.preventDefault(); postServer(e.currentTarget); e.currentTarget.reset(); showPlus(refAva) }}>
                <fieldset className='server-ava'>
                    <input type='file' id='server-ava' name='server-ava' onChange={(e) => { e.target.files[0] ? showAvatar(e, refAva) : showPlus(refAva) }} />
                    <label htmlFor='server-ava'>
                        <div ref={refAva} className="server-ava-add" style={{ backgroundImage: `url(${ava})` }} alt='ava-preview' />
                    </label>
                </fieldset>
                <div className='block-name-submit'>
                    <fieldset className='server-name-public'>
                        <div className='server-name'>
                            <label htmlFor='server-name'>Enter the name of server</label>
                            <input type='text' id='server-name' name='server-name' />
                        </div>
                        <div className='server-public'>
                            <input type='checkbox' id='server-privacy' name='server-privacy' />
                            <label htmlFor='server-privacy'>Make public</label>
                        </div>
                    </fieldset>
                    <button type='submit'>Submit</button>
                </div>
            </form>
        </div>
    );
}

export default ServerForm;
/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import { useDispatch } from 'react-redux';
import { delServers, setServers } from '../../../Store/Reducers/serversReducer';
import './style.css';

function ContextMenu({ server, x, y }) {

    const dispatch = useDispatch();

    async function favoriteServer(id) {
        try {
            const res = await fetch(`/servers/${id}`, {
                method: 'PATCH',
            });
            const result = await res.json();
            res.ok ? dispatch(setServers([...result.filter(item => (item.favorite === 1)), ...result.filter(item => (item.favorite === 0))])) :
                console.log(result);
        }
        catch (err) {
            console.log(err.message);
        }
    }

    async function deleteServer(id) {
        try {
            const res = await fetch(`/servers/${id}`, {
                method: 'PUT',
                body: id,
            });
            res.ok && dispatch(delServers(id))
        }
        catch (err) {
            console.log(err);
        }
    }

    function handleClick(e) {
        if (e.target.classList.contains('show-info')) {
            console.log(server);
        }
        if (e.target.classList.contains('mark-af')) {
            favoriteServer(server.id);
        }
        if (e.target.classList.contains('delete-serv')) {
            deleteServer(server.id);
        }
    }

    return (
        <div className='server-context-menu context-menu' onClick={event => handleClick(event)} style={{ position: 'absolute', top: `${y}px`, left: `${x}px` }}>
            <div className='server-context-menu-container'>
                <div className='server-context-menu-header'>{server.name}</div>
                <div className='server-context-menu-item show-info'>Show info</div>
                <div className='server-context-menu-item mark-af'>{server.favorite ? 'Remove from favorite' : 'Mark as favorite'}</div>
                <div className='server-context-menu-item delete-serv'>Delete</div>
            </div>
        </div>
    );
}

export default ContextMenu;
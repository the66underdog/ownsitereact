import React from 'react';
import './style.css';

function ServerInfoMenu({ name, priv, x, y }) {

    return (
        <div className='server-info-menu context-menu' style={{ position: 'absolute', top: `${y}px`, left: `${x}px` }}>
            <div className='server-info-menu-text'>
                <div>Name: {name}</div>
                <div>Permission: {priv ? 'public' : 'private'}</div>
            </div>
        </div>
    )
}

export default ServerInfoMenu;
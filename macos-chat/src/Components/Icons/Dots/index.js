/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import './style.css'

function MacosDots() {
    
    return (
        <div className="dots">
            <img src="Images/DotRed.png" />
            <img src="Images/DotYellow.png" />
            <img src="Images/DotGreen.png" />
        </div>
    );
}

export default MacosDots;
/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from 'react';
import ServerInfoMenu from '../ServerInfoMenu';
import ContextMenu from '../ContextMenu';
import './style.css';
import { useSelector } from 'react-redux';

function ServersList() {

    const servers = useSelector(state => state.serv.servers);

    const [serverInfo, setServerInfo] = useState(null);
    const [serverContextMenu, setServerContextMenu] = useState(null);
    const [chatActive, setChatActive] = useState(null);

    function mouseOverServerInfo(e) {
        if (e.target.closest('img')) {
            const idx = +e.target.getAttribute('alt');
            for (let server of servers) {
                if (server.id === idx) {
                    setServerInfo(<ServerInfoMenu name={server.name} priv={server.privacy} x={e.pageX} y={e.pageY} />);
                    break;
                }
            }
        }
    }

    function mouseOutServerInfo(e) {
        if (e.target.closest('img')) {
            setServerInfo(null);
        }
    }

    function clickContextMenu(e) {
        if (!e.target.classList.contains('context-menu')) {
            setServerContextMenu(null);
            document.body.removeEventListener('click', clickContextMenu);
        }
    }

    function contextMenu(e) {
        e.preventDefault();
        if (e.target.closest('img')) {
            const idx = +e.target.getAttribute('alt');
            if (e.button === 2) {
                for (let server of servers) {
                    if (server.id === idx) {
                        setServerContextMenu(<ContextMenu server={server} x={e.pageX} y={e.pageY} />);
                        document.body.addEventListener('click', clickContextMenu);
                        break;
                    }
                }
            }
        }
    }

    function setChatServerActive(e) {
        if (e.target.closest('img'))
            setChatActive(+e.target.getAttribute('alt'));
    }

    return (
        <>
            <div className={`chat-servers`}
                onMouseOver={e => { mouseOverServerInfo(e) }} onMouseOut={(e) => mouseOutServerInfo(e)} onContextMenu={e => contextMenu(e)} onClick={e => setChatServerActive(e)}>
                {servers.map((server) => (<img key={server.id} src={`Images/${server.ava}`} alt={server.id}
                    className={server.favorite ? server.id === chatActive ? 'cs-fav cs-act' : 'cs-fav' : server.id === chatActive ? 'cs-act' : null} />))}
            </div>
            {serverInfo}
            {serverContextMenu}
        </>
    );
}

export default ServersList;
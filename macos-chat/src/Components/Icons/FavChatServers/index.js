/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from 'react';
import ServerInfoMenu from '../ServerInfoMenu';
import ContextMenu from '../ContextMenu';
import './style.css';

function FavChatServers({ favServers }) {

    const [chatServerInfo, setChatServerInfo] = useState(null);
    const [chatServerContextMenu, setChatServerContextMenu] = useState(null);

    function showChatServerInfo(e, favServers) {
        setChatServerInfo(<ServerInfoMenu name={favServers['server-name']} priv={favServers['server-privacy']} x={e.pageX} y={e.pageY} />)
    }

    function handleBodyClick(e) {
        if (!e.target.classList.contains('context-menu')) {
            setChatServerContextMenu(null);
            document.body.removeEventListener('click', handleBodyClick);
        }
    }

    function showContextMenu(e, favServers, index) {
        e.preventDefault();
        if (e.button === 2) {
            setChatServerContextMenu(<ContextMenu serv={favServers} x={e.pageX} y={e.pageY} idx={index} />);
            document.body.addEventListener('click', handleBodyClick);
        }
    }

    return (
        <>
            <div className='fav-chat-servers' style={{ border: favServers.length === 0 ? 'none' : '1px solid rgba(255, 255, 255, 0.5)' }}>
                {favServers.map((favServer, index) => {
                    return <img key={index} onMouseOver={(event) => { showChatServerInfo(event, favServer) }} onMouseOut={() => setChatServerInfo(null)}
                        onContextMenu={event => showContextMenu(event, favServer, index)} src={`Images/${favServer['server-ava']}`} />
                })}
            </div>
            {chatServerInfo}
            {chatServerContextMenu}
        </>
    );
}

export default FavChatServers;
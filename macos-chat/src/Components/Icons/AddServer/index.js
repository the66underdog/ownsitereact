import React, { useEffect, useState } from 'react';
import ServerForm from '../ServerForm';
import './style.css';

function AddServer() {

    const [formAddServer, setFormAddServer] = useState(false);

    function rotateButton(event) {
        event.currentTarget.classList.toggle('plus-cross');
    }

    function toggleServerForm(e) {
        if (e.key === 'Escape') {
            document.body.removeEventListener('keydown', toggleServerForm)
            setFormAddServer(false);
        }
    }

    useEffect(() => {
        formAddServer && document.body.addEventListener('keydown', toggleServerForm);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [formAddServer]);

    return (
        <>
            <div className="plus-chat">
                <button className="icon-cross" onClick={(e) => { rotateButton(e); setFormAddServer(prev => !prev) }}
                    style={{ backgroundImage: `url(${process.env.PUBLIC_URL + './Images/Cross.png'})` }}>
                </button>
            </div>
            <ServerForm isDisp={formAddServer} />
        </>
    );
}

export default AddServer;
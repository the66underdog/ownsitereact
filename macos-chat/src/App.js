import './style.css';
import React from 'react';
import { Provider } from 'react-redux';
import Icons from './Components/Icons/index';
import store from './Store/rootReducer';

function App() {
  return (
    <Provider store={store}>
      <Icons />
    </Provider>
  );
}

export default App;

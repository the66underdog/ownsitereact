import { createAction, createReducer } from "@reduxjs/toolkit";

const initState = { servers: [] };

export const setServers = createAction('SET_SERVERS');
export const addServers = createAction('ADD_SERVERS');
export const delServers = createAction('DEL_SERVERS');

export default createReducer(initState, {
    [setServers]: (_, action) => { return { servers: [...action.payload] } },
    [addServers]: (prev, action) => { return { servers: [...prev.servers, action.payload] } },
    [delServers]: (prev, action) => { return { servers: prev.servers.filter(item => (item.id !== action.payload)) } }
});
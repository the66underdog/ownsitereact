import {combineReducers, configureStore} from '@reduxjs/toolkit';
import serversReducer from './Reducers/serversReducer.js';

const rootReducer = combineReducers({
    serv: serversReducer,
})

export default configureStore({
    reducer: rootReducer,
})